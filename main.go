package main

import (
    "fmt"
    "os"
    "log"
    "net/http"
)

const version = "local/1.0"

func main() {
port := os.Getenv("PORT")
if port == "" {
        port = "8100"
        log.Printf("Defaulting to port %s", port)
}

host, _ := os.Hostname()
log.Printf("%s port %s vers. %s", host, port, version)

log.Fatal(http.ListenAndServe(fmt.Sprintf("localhost:%s", port), nil))
}
