# giorno

Giorno di nascita
=================

INSTALLAZIONE
=============

Pre-requisiti:
- Pacchetto golang installato
- Pacchetto git installato

Apri una finestra Terminale come utente normale (non root)

Esegui i seguenti comandi (ognuno seguito da Invio):

git clone https://gitlab.com/mbuto11/giorno.git

cd giorno

go mod init giorno

go mod tidy

go build


Nella finestra Terminale esegui il programma

./giorno

Appariranno due messaggi (data e ora saranno diverse):

2023/04/30 11:41:03 Defaulting to port 8100

2023/04/30 11:41:03 Listening on port 8100

In un browser (Chrome, Firefox...) vai alla pagina:

http://localhost:8100

Buon Divertimanto!
